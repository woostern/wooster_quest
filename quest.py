#! python3

# --------------------------------------------------------
# This is a simple template for creating choose-your-own-adventure
# style quests.
# 
# Author: Nathan and Phoebe Wooster
# Date: 3/29/2016
#
# Credits
# ASCII art generator: http://patorjk.com/software/taag
# --------------------------------------------------------

# --------------------------------------------------------
# A choice
# --------------------------------------------------------
class Choice:
    def __init__(self, text, page_num):
        self.text = text
        self.page_num = page_num


# --------------------------------------------------------
# A page
# --------------------------------------------------------
pages = dict()

class Page:
    def __init__(self, num, text, choices=[]):
        self.num = num
        self.text = text
        self.choices = choices
        pages[num] = self

    def display(self):
        print (self.text)
        cnt = 0
        for choice in self.choices:
            cnt += 1
            print (str(cnt) + ") " + choice.text)

        
    def choose(self):
        self.display()
        
        num_choices = len(self.choices)
        if (num_choices == 0):
            print ("""
 _____ _            _____          _ 
|_   _| |          |  ___|        | |
  | | | |__   ___  | |__ _ __   __| |
  | | | '_ \ / _ \ |  __| '_ \ / _` |
  | | | | | |  __/ | |__| | | | (_| |
  \_/ |_| |_|\___| \____/_| |_|\__,_|
""")
            return

        good = False
        while (not good):
            try:
                choice_num = int(input("? "))
            except:
                print ("Please enter a number between 1 and " + str(num_choices))
                continue

            if ((choice_num < 1) or (choice_num > num_choices)):
                print ("Please enter a number between 1 and " + str(num_choices))
            else:
                good = True

        page_num = self.choices[choice_num-1].page_num

        if (not page_num in pages):
            page_num = "default"

        pages[page_num].choose()
            

# --------------------------------------------------------
# All pages
# --------------------------------------------------------

Page(num="default",
     text="""
There is a tear in the space-time continuum and you are lost in space forever.
""",
     choices=[])

Page(num=1,
     text="""
 _____                 _   
|  _  |               | |  
| | | |_   _  ___  ___| |_ 
| | | | | | |/ _ \/ __| __|
\ \/' / |_| |  __/\__ \ |_ 
 \_/\_\\__,_|\___||___/\__|
                           
                           
Who do you want to be?
This choice is super important!
""",
     choices=[Choice("Hamlet", 2),
              Choice("Ophelia", 3)])

Page(num=2,
     text="""
You are now Hamlet.  You eat a big burrito.
""",
     choices=[Choice("Fart", 3),
              Choice("Barf", 4)])

Page(num=3,
     text="""
You are now Ophelia.  You go to the Moon and never come back.
""",
     choices=[])

# --------------------------------------------------------
# Start the quest
# --------------------------------------------------------

pages[1].choose()
